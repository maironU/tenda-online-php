<?php
    include "../models/Conexion.php";
    session_start();
    $custom = session_id();

    $conexion = Conexion::conectar();

    $totalPedido = 0;

    foreach($_SESSION["car"] as $product){
        $totalPedido = $totalPedido + ($product["price"]*$product["cantidad"]);
    }

    if(isset($_POST["phone"]) && isset($_POST["otherphone"]) && isset($_POST["address"]) && isset($_POST["district"])){
        $phone = $_POST["phone"];
        $otherphone = $_POST["otherphone"];
        $address = $_POST["address"];
        $district = $_POST["district"];


        $resultado = $conexion->prepare("INSERT INTO ventasC (ventasC_id, transaction_id, fecha, correo, phone, otherphone, address, district,methodPay, total, status, user_id) VALUES (null, :custom, NOW(), :correo,:phone, :otherphone, :address,:district, :methodPay, :total, 'aprobado', :user_id)");

        $resultado->bindValue(":custom", $custom);
        $resultado->bindValue(":correo", $_SESSION["usuario"]['email']);
        $resultado->bindValue(":phone", $phone);
        $resultado->bindValue(":otherphone", $otherphone);
        $resultado->bindValue(":address", $address);
        $resultado->bindValue(":district", $district);
        $resultado->bindValue(":methodPay", 'contraentrega');
        $resultado->bindValue(":total", $totalPedido);
        $resultado->bindValue(":user_id", $_SESSION['usuario']['user_id']);


        $resultado->execute();

        $idVenta = $conexion->lastInsertId();

        foreach($_SESSION['car'] as $product){
            $resultado = $conexion->prepare("INSERT INTO detalleventasC (id, product_id, ventaC_id, preciounitario, cantidad) VALUES (null, :idProduct, :idVentaC, :price, :cantidad)");

            $resultado->bindValue(":idProduct", $product["product_id"]);
            $resultado->bindValue(":idVentaC", $idVenta);
            $resultado->bindValue(":price", $product["price"]);
            $resultado->bindValue(":cantidad", $product["cantidad"]);

            $resultado->execute();
        }

        $resultadoUpdate = $conexion->prepare("UPDATE ventasC SET status='en proceso' WHERE transaction_id = :idTransaction 
        AND total = :total 
        AND ventasC_id = :idVenta");

        $resultadoUpdate->bindValue(":idTransaction", $custom);
        $resultadoUpdate->bindValue(":total", $totalPedido);
        $resultadoUpdate->bindValue(":idVenta", $idVenta);

        $resultadoUpdate->execute();
        unset($_SESSION["car"]);
        $_SESSION["message"] = "true";

        echo "correcto";
    }else{
        echo "incorrecto";
    }

?>