function addToCar(){
    var id = document.getElementById("pdt").value;
    var selected = document.getElementById("cantidad");
    var cantidad = parseInt(selected.options[selected.selectedIndex].text);

    $.ajax({
        type: 'POST',
        url: 'myCar.php',
        data: {id, btnAction : "add", cantidad},
        success: function(response){
            if(response!="encontrado"){
                Swal.fire({
                    title : 'Producto agregado al carrito',
                    icon: 'success',
                    confirmButtonText: "Ir al carrito",
                    showCancelButton: true,
                    cancelButtonText: "Seguir comprando"
                }).then((result) => {
                    if(result.value) {
                        location.href = "car.php";
                    }else{
                        location.href = "index.php";
                    }
                })

                document.getElementById("cantCar").innerHTML = response;
            }else{
                Swal.fire({
                    icon: 'error',
                    text: 'El producto ya esta agregado en el carrito',
                  })
            }
        }
    })
}


    