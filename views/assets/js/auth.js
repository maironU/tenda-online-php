$(document).ready(function(){
    $("#loginForm").bind("submit", function(){
        $.ajax({
            type: $(this).attr("method"),
            url: $(this).attr("action"),
            data: $(this).serialize(),
            success: function(response){
                if(response.estado == "true"){
                    localStorage.setItem("modal", "false");
                    window.location.href = "index.php";
                }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'Usuario o contraseña incorrectas',
                      })
                    $('input[type="password"]').val('');
                }
            }
        });
        return false;
    });

    $("#registerForm").bind("submit", function(){
        $.ajax({
            type: $(this).attr("method"),
            url: $(this).attr("action"),
            data: $(this).serialize(),
            success: function(response){
                if(response.register == "true"){
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Usuario creado con exito',
                        showConfirmButton: true,
                      }).then((result) => {
                          if(result.value){
                              window.location.href = "login.php";
                          }
                      })

                    $("#registerForm")[0].reset();
                }else{
                    console.log("usuario o password incorrectas");
                    $('input[type="password"]').val('');
                }
            }
        });
        return false;
    });
});
