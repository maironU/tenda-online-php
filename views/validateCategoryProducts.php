<?php 
    require_once "../controllers/productController.php";

    if(isset($_GET["c"])){
        $nameCategory = $_GET["c"];
    }else{
        $nameCategory = "";
    }

    if(isset($_GET["search"])){
        $search = $_GET["search"];
    }else{
        $search = "";
    }

    if(isset($_GET["c"])){
        $products = productController::getProductsForCategory($_GET["c"]);
        include "../views/index.php";
    }else if(isset($_GET["search"])){
        $products = productController::getProductsForName($_GET["search"]);
        include "../views/index.php";
    }else{
        $products = productController::getProducts();
    }

?>