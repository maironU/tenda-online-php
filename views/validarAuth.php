<?php

    include "../controllers/userController.php";
    include "../helps/help.php";

    session_start();

    header("Content-type: application/json");

    $url = $_SERVER["REQUEST_URI"];   
    $arrayUrl = explode('?', $url);
    
    $CaM = $arrayUrl[count($arrayUrl) - 1];
    $arrayCaM = explode('=', $CaM);

    $controlador = $arrayCaM[0];
    $metodo = $arrayCaM[1];

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        switch ($controlador."=".$metodo) {
            case "c=login":
                if(isset($_POST["email"]) && isset($_POST["password"])){
                    $email = validar_campo($_POST["email"]);
                    $password = validar_campo($_POST["password"]);

                    if(userController::login($email, $password)){
                        $resultado = array("estado" => "true");

                        $usuario = userController::getUser($email, $password);

                        $_SESSION["usuario"] = array(
                            "user_id"          => $usuario["user_id"],
                            "name"          => $usuario["name"],
                            "lastname"      => $usuario["lastname"],
                            "email"         => $usuario["email"],
                        );

                        return print(json_encode($resultado));
                    }

                    $resultado = array("estado" => "false");
                    return print(json_encode($resultado));
                }
                
            case "c=register":
                if(isset($_POST["name"]) && isset($_POST["lastname"]) && isset($_POST["email"]) && isset($_POST["password"])){
                    
                    $name = validar_campo($_POST["name"]);
                    $lastname = validar_campo($_POST["lastname"]);
                    $email = validar_campo($_POST["email"]);
                    $password = validar_campo($_POST["password"]);

                    if(userController::register($name, $lastname, $email, $password)){
                        $resultado = array("register" => "true");
                        return print(json_encode($resultado));
                    };
                    $resultado = array("register" => "false");
                    return print(json_encode($resultado));
                }
            }
        }

?>