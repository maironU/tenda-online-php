<?php
    require_once "partials/head.php";
    require_once "partials/header.php";
    require_once "../controllers/ventascController.php";
    require_once "../config/config.php";
    
    if(!isset($_SESSION["usuario"])){
        header("location: index.php");
    }else{
        $orders = ventascController::getOrdersForUser($_SESSION["usuario"]["user_id"]);
    }
?>

<ul class="type-order margin-top-sm d-flex ml-auto p-0 ml-4">
    <li class="mr-1" id="paypal"><a href="paypal.php" class="nav-link text-dark">Paypal</a></li>
    <li class="category-active"><a href="contraentrega.php" class="nav-link text-dark">Contraentrega</a></li>
</ul>

<?php if(!empty($orders)){ ?>
    <div class="container pl-0 pr-0">
        <h3 class="text-center">Mis Pedidos</h3>
        <hr>
        <?php foreach($orders as $order){ ?>
            <div class="my-order mw-100vw mt-4 d-flex align-items-center">
                <div class="my-order__img mr-4">
                    <img width="100%" height="100%" src="assets/logos/order.png" alt="">
                </div>
                <div class="my-order__details d-flex flex-column justify-content-center">
                    <span>Total: <code>$<?php echo $order["total"] ?></code></span>
                    <span>Fecha: <?php echo $order["fecha"] ?></span>
                    <span>Estado : <?php echo $order["status"] ?></span>
                    <a href="orderDetailsC.php?order=<?php echo $order["ventasC_id"]?>">Ver</a>
                </div>
            </div>
        <?php } ?>
    </div> 
<?php }else{?>
    <div class="text-center margin-top-lg">
        <h3>No tienes compras o pedidos pendientes en contraentrega</h3>
        <a href="index.php" class="btn btn-primary">Comprar</a>
    </div>
<?php }?>

<?php require_once "partials/scripts.php" ?>