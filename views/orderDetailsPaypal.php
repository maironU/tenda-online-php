<?php
    include "../config/config.php"; 
    include "../models/Conexion.php";

    session_start();

    $ClientId = PayPalClientId;
    $Secret = PayPalSecret;

    $Login = curl_init(PayPalBaseUrl."/v1/oauth2/token");

    curl_setopt($Login, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($Login, CURLOPT_USERPWD, $ClientId.":".$Secret);
    curl_setopt($Login, CURLOPT_POSTFIELDS, "grant_type=client_credentials");

    $response = curl_exec($Login);

    $objResponse = json_decode($response);

    $AccessToken = $objResponse->access_token;

    $venta = curl_init(PayPalBaseUrl."/v1/payments/payment/".$_GET["paymentID"]); 

    curl_setopt($venta, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer ".$AccessToken));

    curl_setopt($venta, CURLOPT_RETURNTRANSFER, TRUE);
    
    $responseVenta = curl_exec($venta);

    $objDatosTransaction=json_decode($responseVenta);

    $state = $objDatosTransaction->state;
    $email = $objDatosTransaction->payer->payer_info->email;

    $totalPaypal = $objDatosTransaction->transactions[0]->amount->total;
    $currency = $objDatosTransaction->transactions[0]->amount->currency;
    $custom = $objDatosTransaction->transactions[0]->custom;
    $methodPay = $objDatosTransaction->payer->payment_method;

    var_dump($methodPay);

    curl_close($venta);
    curl_close($Login);
    
    if($state == 'approved'){
        $mensajePaypal = true;

       $conexion = Conexion::conectar();

        $resultado = $conexion->prepare("INSERT INTO ventas (venta_id, transaction_id, paypalDatos, fecha, correo, methodPay, total, status, user_id) VALUES (null, :custom, :paypalDatos, NOW(), :correo, :methodPay, :total, 'aprobado', :user_id)");

        var_dump($_SESSION['usuario']);

        $resultado->bindValue(":custom", $custom);
        $resultado->bindValue(":paypalDatos", $responseVenta);
        $resultado->bindValue(":correo", $_SESSION["usuario"]['email']);
        $resultado->bindValue(":methodPay", $methodPay);
        $resultado->bindValue(":total", $totalPaypal);
        $resultado->bindValue(":user_id", $_SESSION['usuario']['user_id']);


        $resultado->execute();

        $idVenta = $conexion->lastInsertId();

        foreach($_SESSION['car'] as $product){
            $resultado = $conexion->prepare("INSERT INTO detalleventas (id, product_id, venta_id, preciounitario, cantidad) VALUES (null, :idProduct, :idVenta, :price, :cantidad)");

            $resultado->bindValue(":idProduct", $product["product_id"]);
            $resultado->bindValue(":idVenta", $idVenta);
            $resultado->bindValue(":price", $product["price"]);
            $resultado->bindValue(":cantidad", $product["cantidad"]);

            $resultado->execute();
        }

        $resultadoUpdate = $conexion->prepare("UPDATE ventas SET status='completo' WHERE transaction_id = :idTransaction 
        AND total = :total 
        AND venta_id = :idVenta");

        $resultadoUpdate->bindValue(":idTransaction", $custom);
        $resultadoUpdate->bindValue(":total", $totalPaypal);
        $resultadoUpdate->bindValue(":idVenta", $idVenta);

        $resultadoUpdate->execute();

        unset($_SESSION["car"]);

        $_SESSION["message"] = "true";
        header("location: messageSuccess.php?m=1");    
    }else{
        $_SESSION["message"] = true;
        header("location: messageSuccess.php?m=0");    
    }
?>
