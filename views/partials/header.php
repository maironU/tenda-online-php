<?php
    require_once "../controllers/categoryController.php";
    require_once "validateCategoryProducts.php";
    
    $categories = categoryController::getCategories();

    if(!isset($_SESSION)){
        session_start();
    }
?>

<nav class="navbar navbar-dark bg-dark text-dark navbar-expand-lg fixed-top justify-content-between p-sm-1 p-md-1 p-lg-2 mx-100vw">
    <a href="index.php" class="navbar-brand">logo</a>

    <form style="width: 40%" id="search" action="search" class="my-2 my-lg-0 search-collapse position-relative" method="GET">
        <input class="form-control form-control-sm" placeholder="Buscar" aria-label="Search" name="search" autofocus>
        <button class="bg-white position-absolute btn p-0" style="right:5px; bottom:4px" type="submit"><i type="submit" class="fa fa-search text-secondary"></i></button>
    </form>

    <a href="car.php" class="text-md-center text-sm-center text-decoration-none d-flex justify-content-center align-items-center">
        <i style="font-size: 30px" class="fa fa-shopping-cart"><span style="font-size: 20px" class="size text-primary" id="cantCar"><?php 
            if(isset($_SESSION["car"])){
                echo count($_SESSION["car"]);
            }else{
                echo "0";
            }

        ?></span></i>
    </a>

    <button class="navbar-toggler p-1" data-toggle="collapse" data-target ="#navbarNav"><span class="navbar-toggler-icon"></span></button>


    <div class="collapse navbar-collapse flex-grow-0" id="navbarNav">
        <ul class="navbar-nav justify-content-end align-items-lg-center">

             <!-- DROP DOWN CATEGORIAS -->
             <div class="btn-group__category mr-3 ml-md-2 ml-sm-2">
                <button class="btn btn-dark dropdown-toggle pt-1 pb-1 pl-0 text-white-50" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Categorias
                </button>

                <div class="dropdown-menu text-muted " aria-labelledby="dropdownMenuLink">
                    <?php foreach ($categories as $category) {?>
                        <a class="dropdown-item" href="<?php echo $category["name"] ?>"><?php echo $category["name"] ?></a>
                    <?php }?>
                </div>
            </div>

            <?php if(!isset($_SESSION["usuario"])){ ?>
                <li class="nav-item mr-3 ml-md-2 ml-sm-2"><a href="login.php" class="nav-link ">Login</a></li>
                <li class="nav-item mr-3 ml-md-2 ml-sm-2"><a href="register.php" class="nav-link">Register</a></li>
            <?php } ?>

            <div class="align-items-center">
            <!-- DROP DOWN CUENTA -->
            <?php if(isset($_SESSION["usuario"])){?>
                <div class="btn-group__category mr-5 ml-md-2 ml-sm-2">
                    <button type="button" class="btn btn-dark dropdown-toggle  pt-1 pb-1 pl-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Cuenta
                    </button>
                    <div class="dropdown-menu  dropdown-menu-md-left">
                        <a class="dropdown-item" type="button">Perfil</a>
                        <a class="dropdown-item" href="paypal.php" type="button">Mis pedidos</a>
                        <a class="dropdown-item" href="closeSession.php" type="button">Cerrar sesión</a>
                    </div>
                </div>
            <?php } ?>
        </div>
        </ul>
    </div>
</nav>


