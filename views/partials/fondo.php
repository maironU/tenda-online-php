<header class="header d-flex justify-content-center">
    <div class="title-header">
        <h2 class="text-light">Bienvenido a la tienda online</h2>
        <a href="#" class="btn btn-primary">Empezar a comprar</a>
    </div>
</header>