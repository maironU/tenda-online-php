<div class="myCar__resumen-order ml-lg-4 pt-lg-3 pb-3">
    <h4 class="text-center">Resumen Pedido</h4>
    <div class="container d-flex flex-column myCar__resumen-order__sumary justify-content-between">
        <div class="d-flex flex-column">
            <span class="pt-2">Numero de productos: <?php
                echo $cantidad;
                ?> <hr></span>
            <span class="pt-2">Envio: Gratis <hr></span>
            <span class="pt-2">Total: <code>$<?php echo $totalPedido ?></code><hr></span>
      