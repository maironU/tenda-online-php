<?php
    include "partials/head.php";
    session_start();

    if(isset($_GET['m']) && isset($_SESSION["message"])){
        $mensaje = $_GET["m"];
        if($mensaje == '1'){
?>
            <div class="row jumbotron w-md-50 text-center mx-auto flex-column">
                <h1 class="h2 alert alert-success">Tu compra ha sido exitosa</h1>
                <hr class="my-4">
                <p class="lead">Gracias por comprar en nuestra tienda</p>
                <a href="index.php" class="btn btn-primary">Volver</a>
            <div id="paypal-button"></div> 
            
            <?php unset($_SESSION["message"]); ?>        
    <?php
    }else{
        header("location: car.php");
    }
}else{
    header("location: car.php");
}
include "partials/scripts.php";

?>