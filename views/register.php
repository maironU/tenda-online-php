<?php
    include "partials/head.php";
    include "partials/header.php";

    if(isset($_SESSION["usuario"])){
        header("location: index.php");
    }
?>

<div class="row justify-content-center align-items-center ml-0 mr-0">
    <div style="width: 310px;">
        <form id="registerForm" action="validarAuth.php?c=register" method="POST" class="form-login" role="form">
            <legend>Registrarse</legend>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Name" required autofocus name="name">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="LastName" required name="lastname">
            </div>
            <div class="form-group">
                <input type="email" class="form-control" placeholder="Email"  required name="email">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="Password" required name="password">
            </div>
            <button type="submit" class="btn btn-success w-100">
                Registrarse
            </button>
            <div class="d-flex flex-column text-center mt-2">
                <span>Ya tienes una cuenta?</span>
                <a href="login.php" class="">Iniciar sesión</a>
            </div>
        </form>
    </div>
</div>

<?php include "partials/scripts.php" ?>