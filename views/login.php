<?php
    include "partials/head.php";
    include "partials/header.php";

    if(isset($_SESSION["usuario"])){
        header("location: index.php");
    }
?>
<div class="row justify-content-center align-items-center ml-0 mr-0 flex-column">

    <?php if(isset($mensaje)){ ?>
            <div class="alert alert-danger"><?php echo $mensaje ?></div>
    <?php } ?>

    <div style="width: 310px;">
        <form action="validarAuth.php?c=login" method="POST" class="form-login" id="loginForm" role="form">
            <legend>Iniciar sesión</legend>
            <div class="form-group">
                <input type="email" class="form-control" placeholder="Email" autofocus name="email">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="Password" name="password">
            </div>
            <button type="submit" class="btn btn-success w-100">
                Ingresar
            </button>
            <div class="d-flex flex-column text-center mt-2">
                <span>¿No tienes una cuenta?</span>
                <a href="register.php" class="">Registrarse</a>
            </div>
        </form>
    </div>
</div>

<?php include "partials/scripts.php" ?>