<?php
    include "../config/config.php";
    include "../controllers/productController.php";

    session_start();
    
    if(isset($_POST["btnAction"])){
        switch($_POST["btnAction"]){
            case 'add':
                $product = array();

                if(is_numeric(openssl_decrypt($_POST["id"], COD, KEY))){
                    $id = openssl_decrypt($_POST["id"], COD, KEY);

                    $product = productController::getProductForId($id);
                    $cantidad = $_POST["cantidad"];

                }    
                
                if(!isset($_SESSION["car"])){
                    $_SESSION["car"][0] = array(
                        "product_id" => $product[0]["product_id"],
                        "name" => $product[0]["name"],
                        "price" => $product[0]["price"],
                        "url" => $product[0]["url"],
                        "cantidad" => $cantidad
                    );

                    echo count($_SESSION['car']);
                }else{

                    $idProduct = array_column($_SESSION['car'], 'product_id');
                    if(in_array($id, $idProduct)){
                        echo "encontrado";
                    }else{
                        $newProduct = array(
                            "product_id" => $product[0]["product_id"],
                            "name" => $product[0]["name"],
                            "price" => $product[0]["price"],
                            "url" => $product[0]["url"],
                            "cantidad" => $cantidad
                        );
                        array_push($_SESSION['car'], $newProduct);
                        echo count($_SESSION['car']);
                    }
                }
                break;

            case 'delete':

                echo $_POST["id"];

                if(is_numeric(openssl_decrypt($_POST["id"], COD, KEY))){
                    $id = openssl_decrypt($_POST["id"], COD, KEY);
                    
                    foreach($_SESSION['car'] as $index=>$product){
                        if($product['product_id'] == $id){
                            unset($_SESSION['car'][$index]);
                            header("location: car.php");
                        }
                    }

                    if(count($_SESSION["car"]) == 0){
                        unset($_SESSION["car"]);
                    }
                } 
                
                break;
        }
    }
?>