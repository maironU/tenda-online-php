<?php 
    if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 

    if(!isset($_SESSION['usuario'])){
        $mensaje = "Por favor primero inicia sesión";
        include("login.php");
    }else{
        header("location: pay.php");
    }
?>
