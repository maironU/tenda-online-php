 <?php 
    include "partials/head.php";
    include "partials/header.php";
    include "partials/fondo.php";
?>

<!--- SI EXISTE UNA SESSION -->
<?php if(isset($_SESSION["usuario"])){ ?>
    <div class="text-right">
        <span class="mr-3">Hola <?php echo $_SESSION["usuario"]["name"]." ". $_SESSION["usuario"]["lastname"] ?>!</span>
    </div>

<!-- Modal -->
    <?php include "modal.php" ?>
<?php } ?>

<h3 class="text-center mt-4 text-uppercase">
    <?php 

    if(!empty($nameCategory) && !empty($products)){
        echo $nameCategory;
    }else if(!empty($nameCategory) && empty($products)){
        echo "no existe la categoria";
    }else if(!empty($search) && !empty($products)){
        echo "Resultados para la busqueda '".$search."'";
    }else if (!empty($search) && empty($products)){
        echo "No se encontraron resultados";
    }else{
        echo "Todos los productos";
    }
    ?>
</h3>


<div class="product h-auto">
    <?php foreach ($products as $product) {?>
        <a href="details.php?product=<?php echo $product["product_id"] ?>" class="product__item product__effect">
            <div class="product__img">
                <img src="assets/img_products/<?php echo $product["url"]?>" alt="">
            </div>
            <span class="product__name"><?php echo $product["name"] ?></span>
            <span class="product__price"><code>$<?php echo $product["price"] ?></code></span>
        </a>
    <?php }?>
</div>
<?php include "partials/scripts.php" ?>


    