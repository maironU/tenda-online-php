<?php

    include "partials/head.php";
    include "partials/header.php";
    include "../config/config.php";

    if(isset($_SESSION['usuario']) && !isset($_SESSION['car'])){
        header("location: car.php");
    }
?>

<?php 
    $total = 0;
    $id=session_id();
    foreach($_SESSION['car'] as $product){
        $total = $total + ($product['price']*$product['cantidad']);
    }
?>
    <div class="row jumbotron w-md-50 text-center mx-auto flex-column">
        <h1 class="h2">PASO FINAL!</h1>
        <hr class="my-4">
        <p class="lead">Estas a punto de pagar la cantidad de:</p>
        <code><span>$<?php echo $total ?></span></code>
        <span class="mb-3">Elige tu metodo de pago</span>
        <div id="paypal-button"></div>
        <a href="payContraentrega.php" class="btn btn-primary btn-sm text-light mx-auto d-flex align-items-center justify-content-center" style="border-radius: 20px; min-width: 150px; height:25px"><span class="text-center">Contraentrega</span></a>
    </div>

    <script src="https://www.paypalobjects.com/api/checkout.js"></script>

    <script>
        paypal.Button.render({
        env: '<?php echo PayPalENV; ?>',
        client: {
            sandbox: '<?php echo PayPalClientId; ?>'
        },
        payment: function (data, actions) {
            return actions.payment.create({
            transactions: [
                {
                amount: {total: '<?php echo $total; ?>',currency: 'MXN'},
                description: "Compra de productos en tienda online: <?php echo $total ?>",
                custom: "<?php echo $id?>"
                }
            ]
            });
        },
        onAuthorize: function (data, actions) {
            return actions.payment.execute()
            .then(function () {
                window.location="orderDetailsPaypal.php?paymentToken = " + data.paymentToken +"&paymentID="+data.paymentID;
            })
        }
        }, '#paypal-button');
    </script>

<?php include "partials/scripts.php" ?>
