<?php
    include "partials/head.php";
    include "partials/header.php";
    include "../config/config.php";

    if(!isset($_SESSION)){
        session_start();
    }
?>
<div class="row ml-0 mr-0">
    <?php 
    if(isset($_SESSION["car"])){
        $products = $_SESSION["car"];
    ?>
        <a href="emptyCar.php" class=" btn btn-warning ml-auto mr-4 text-light">Vaciar carrito</a>
        <h1 class="text-center mb-3 w-100">Carrito de compras</h1>
        <div class="myCar">
        <?php 
            $cantidad = 0;
            $totalPedido = 0;
            foreach ($products as $indice=>$product){ ?>
                <div class="myCar__products" style="word-break: break-all;">
                    <div class="myCar__products__item d-flex align-items-center mb-4">

                    <div class="myCar_image-product">
                        <img width="100%" height="100%" src="assets/img_products/<?php echo $product["url"] ?>" alt="">
                    </div>

                        <div class="myCar__details d-flex flex-column justify-content-center ml-4 mr-4">
                            <span><?php echo $product["name"] ?></span>
                            <span><code>$<?php echo $product["price"] ?></code></span>
                            <span>cantidad: <?php echo $product["cantidad"] ?></span>
                        </div>

                        <form action="myCar.php" method="POST">
                            <input type="hidden" name="id" value="<?php echo openssl_encrypt($product["product_id"], COD, KEY)?>">

                            <button type="submit" value="delete" class="btn btn-lg size-big" id="delete" name="btnAction">
                                <i class="fa fa-trash-o"></i>
                            </button> 
                        </form>
                    </div>
                    <hr>
                </div>
        <?php 
            $cantidad = $cantidad + $product["cantidad"];
            $totalPedido = $totalPedido + ($product["price"]*$product["cantidad"]);
        } 
        ?>
    </div>

    <?php include "partials/resumenOrder.php" ?>
                <div class="proccess_buy fixed-md-bottom">
                    <a href="verifySessionToPay.php" class="btn btn-danger w-100 p-sm-3">Procesar compra</a>
                </div>
            </div>
        </div>
    </div>

    <?php
        }else{
    ?>
    <div class="row mt-0 justify-content-center align-items-center mx-auto">
            <div class="col-8 text-center">
                <div>
                    <i style="font-size: 80px; color:red" class="fa fa-shopping-cart"></i>
                </div>
                <h2 class="mb-2 size">No hay productos en el carrito</h2>
                <a href="index.php" class="btn btn-primary">Comprar</a>
            </div>
        </div>
    <?php } ?>
</div>

<?php include "partials/scripts.php" ?>
