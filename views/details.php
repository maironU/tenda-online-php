<?php
    include "partials/head.php";
    include "partials/header.php";
    require_once "../controllers/productController.php";
    include "../config/config.php";

    $product = productController::getProductForId($_GET["product"]);
?>

<?php if(!empty($product)){ ?>
    <div class="row align-items-center justify-content-center ml-0 mr-0 pb-4">
        <h3 class="text-center w-100 mb-3">Detalles del producto
        </h3> 

        <div class="col-md-4 col-sm-8 ml-3 d-flex justify-content-center">
            <img class="w-100 h-50" src="assets/img_products/<?php echo $product[0]["url"] ?>" alt="">
        </div>

        <div class="col-md-6 col-sm-8 col-lg-5 d-flex flex-column mt-4">
            <h4 class="text-center mt-3"><?php echo $product[0]["name"] ?></h4>

            <p class="text-justify"><?php echo $product[0]["details"] ?></p>

            <span class="details__price product__price text-center"><code>$<?php echo $product[0]["price"] ?></code></span>

            <div class="details__send mt-3 p-4 margin-bottom-lg">
                <span class="details__send__title text-success"><i class="fa fa-car mr-3"></i>Envio gratis</span>
            </div>

            <div class="row mt-0 justify-content-between mt-4 ml-0 p-0 bottom-fixed mr-0">
                <div class="col-3 pl-0 pr-0">
                    <select name="" id="cantidad" class="h-100 w-100" style="font-size: 30px">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                    </select>   
                    </div>
                    <div class="col-9 pr-0 pl-0">
                        <input type="hidden" id="pdt" value="<?php echo openssl_encrypt($product[0]["product_id"], COD, KEY) ?>">

                        <button type="button" value="add" class="details__add-to-car" id="add" onclick="addToCar()">
                                Añadir al carrito
                        </button>
                </div>
            </div>
        </div>
    </div>
<?php }else{?>
    <h1>No se encontró el producto</h1>
<?php } ?>

<?php include "partials/scripts.php" ?>