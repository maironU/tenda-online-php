<?php 
    require_once "partials/head.php";
    require_once "partials/header.php";
    require_once "../config/config.php";
    require_once "../controllers/detalleventaspController.php";

    $venta_id = $_GET["order"];

    $products = detalleventaspController::getOrdersForVenta($venta_id);

?>
<?php if(!empty($products)){ ?>
    <div class="margin-top-lg">
        <a href="paypal.php" class="ml-3 text-primary"><i class="fa fa-chevron-left"></i> Volver</a>
        <h3 class="text-center mt-3">Detalles del pedido</h3>
        <?php foreach($products as $product){ ?>
            <div class="my-order mw-100vw mt-4 d-flex align-items-center">
                <div class="my-order__img mr-4">
                    <img width="100%" height="100%" src="assets/img_products/<?php echo $product["url"] ?>" alt="">
                </div>
                <div class="my-order__details d-flex flex-column justify-content-center">
                    <span>Cantidad de productos : <?php echo $product["cantidad"] ?></span>
                    <span>Precio unitario: <code>$<?php echo $product["price"] ?></code></span>
                </div>
            </div>
        <?php }?>
    </div>
<?php } ?>

<?php require_once "partials/scripts.php" ?>