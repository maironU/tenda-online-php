<?php
    include "partials/head.php";
    session_start();

    if(isset($_SESSION["car"]) && isset($_SESSION["usuario"])){
?>  
    <nav class="navbar navbar-dark bg-dark p-4">
        <div class="container">
            <a href="index.php" class="navbar-brand">logo</a>

            <div class="navbar-title text-light">
                <span><i class="fa fa-lock mr-3"></i>COMPRA SEGURA</span>
            </div>
        </div>
    </nav>

    <h3 class="card-title text-center mt-4 h4">Proceso de pago contraentrega</h3>
    <h4 class="text-center text-small">(Por favor llene los siguientes campos para procesar con el pedido)</h4>

    <section class="card-main-step container d-flex justify-content-between flex-wrap">
        <div class="container-form w-60 mt-4 mb-4">
            <form action="" id="formOrder" class="d-flex flex-wrap justify-content-md-between justify-content-center ">
                <div class="form-group w-45 w-sm-70">
                    <label for="name">Nombre</label>
                    <input type="text" class="form-control" name="name" id="name" value =<?php echo $_SESSION["usuario"]["name"] ?>>
                </div>
                <div class="form-group w-45 w-sm-70">
                    <label for="lastname">Apellido</label>
                    <input type="text" class="form-control" name="lastname" id="lastname" value =<?php echo $_SESSION["usuario"]["lastname"] ?>>
                </div>

                <div class="form-group w-45 w-sm-70">
                    <label for="phone">Número de celular</label>
                    <input type="number" class="form-control" name="phone" id="phone" max="10" onkeyup="formatNumber(id)">

                    <span id="error-phone" class="text-danger"></span>
                </div>

                <div class="form-group w-45 w-sm-70">
                    <label for="otherphone">Otro Numero de celular(Opcional)</label>
                    <input type="number" class="form-control" name="otherphone" id="otherphone" onkeyup="formatNumber(id)">

                    <span id="error-otherphone" class="text-danger"></span>
                </div>

                <div class="form-group  w-45 w-sm-70">
                    <label for="address">Dirección</label>
                    <input type="text" class="form-control" name="address" id="address">
                </div>

                <div class="form-group  w-45 w-sm-70">
                    <label for="district">Barrio</label>
                    <input type="text" class="form-control" name="district" id="district">
                </div>
            </form>
        </div>

        <?php 
            $products = $_SESSION["car"];
            $cantidad = 0;
            $totalPedido = 0;

            foreach($products as $product){
                $cantidad = $cantidad + $product["cantidad"];
                $totalPedido = $totalPedido + ($product["price"]*$product["cantidad"]);
            }
        ?>

        <?php include "partials/resumenOrder.php" ?>
        <div class="proccess_buy fixed-md-bottom">
                    <button type="button" onClick="contraentregaPay()" class="btn btn-danger w-100 p-sm-3">Finalizar compra</button>
                </div>
            </div>
        </div>
    </section>

    <?php }else{
        header("location:car.php");
    } ?>

<?php include "partials/scripts.php" ?>