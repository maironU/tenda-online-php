<?php
    include "../models/Product.php";

    class productController
    {
        public static function getProducts(){
            return Product::getProducts();
        }

        public static function getProductsForCategory($category){
            return Product::getProductsForCategory($category);
        }

        public static function getProductsForName($search){
            return Product::getProductsForName($search);
        }

        public static function getProductForId($id){
            return Product::getProductForId($id);
        }
    }
?>