<?php 

    include "../models/User.php";

    class userController
    {
        public static function login($email, $password){
            return User::login($email, $password);
        }   

        public static function register($name, $lastname, $email, $password){
            return User::register($name, $lastname, $email, $password);
        }

        public static function getUser($email, $password) {
            return User::getUser($email, $password);
        }
    }

?>