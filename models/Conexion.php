<?php

    class Conexion
    {   
        public static function conectar(){
            try {
                include "../config/database.php";

                $conexion = new PDO($config["driver"].":host=".$config["host"].";dbname=".$config["database"],$config["user"], $config["pass"] );

                return $conexion;

            }catch(PDOException $e) {
                die($e->getMessage());
            }
        }
    }
?>