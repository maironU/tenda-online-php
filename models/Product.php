<?php
    require_once "Conexion.php";

    class Product
    {
        private static $conexion;
        private static $products = array();

        public static function getConexion(){
            self::$conexion = Conexion::conectar();
        }

        public static function getProducts(){
            self::getConexion();

            $query = "SELECT product_id, name, price, url FROM product ORDER BY RAND()";

            $resultado = self::$conexion->prepare($query);
            $resultado->execute();

            while ($fila = $resultado->fetch()) {
                self::$products[]=$fila;
             }     

            return self::$products;
        }

        public static function getProductsForCategory($category){
            self::getConexion();

            $query = "SELECT p.product_id, p.name, p.price, p.url FROM product as p INNER JOIN category as c on p.category_id = c.category_id WHERE c.name = :category";

            $resultado = self::$conexion->prepare($query);
            $resultado->bindValue(":category", $category);
            $resultado->execute();

            while ($fila = $resultado->fetch()) {
                self::$products[]=$fila;
             }     

            return self::$products;
        }

        public static function getProductsForName($search){
            self::getConexion();

            if(!empty($search)){
                /* Verifico si la busqueda es una categoria si es asi retorno todos los productos de esa categoria*/
                $products = self::getProductsForCategory($search);

                if(!empty($products)){
                    return $products;
                }else{ 
                    /* SI no es categoria, simplemente busco con LIKE*/ 
                    $query = "SELECT product_id, name, price, url FROM product WHERE name LIKE '%$search%'";
                    $resultado = self::$conexion->prepare($query);
                    $resultado->execute();

                    if($resultado->rowcount() > 0){
                        while ($fila = $resultado->fetch()) {
                            self::$products[]=$fila;
                        }
                    }else{
                        return [];
                    }
                }    
                return self::$products;
            }
        }

        public static function getProductForId($id){
            self::getConexion();

            $query = "SELECT * FROM product WHERE product_id = :id";

            $resultado = self::$conexion->prepare($query);
            $resultado->bindValue(":id", $id);

            $resultado->execute();

            if($resultado ->rowcount() > 0){
                $product[] = $resultado->fetch();
                return $product;
            }else{
                return [];
            }
        }
    }
?>
