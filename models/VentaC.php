<?php
    require_once "Conexion.php";

    class VentaC
    {
        private static $conexion;
        private static $orders = array();

        public static function getConexion(){
            self::$conexion = Conexion::conectar();
        }

        public static function getOrdersForUser($user_id){
            self::getConexion();

            $query = "SELECT * FROM ventasc WHERE user_id = :user_id";
            $resultado = self::$conexion->prepare($query);
            $resultado->bindValue("user_id", $user_id);

            $resultado->execute();

            if($resultado->rowcount() > 0){
                while ($fila = $resultado->fetch()) {
                    self::$orders[]=$fila;
                }
            }else{
                return [];
            }     

            return self::$orders;
        }
    }
?>