<?php
    require_once "Conexion.php";

    class DetalleVentaP
    {
        private static $conexion;
        private static $products = array();

        public static function getConexion(){
            self::$conexion = Conexion::conectar();
        }

        public static function getOrdersForVenta($venta_id){
            self::getConexion();

            $query = "SELECT * FROM detalleventas as d INNER JOIN product as p ON d.product_id = p.product_id  WHERE venta_id = :venta_id";

            $resultado = self::$conexion->prepare($query);
            $resultado->bindValue("venta_id", $venta_id);

            $resultado->execute();

            if($resultado->rowcount() > 0){
                while ($fila = $resultado->fetch()) {
                    self::$products[]=$fila;
                }
            }else{
                return [];
            }     

            return self::$products;
        }
    }
?>