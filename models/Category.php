<?php
    require_once "Conexion.php";

    class Category {
        private static $conexion;
        private static $categories = array();

        public static function getConexion(){
            self::$conexion = Conexion::conectar();
        }

        public static function getCategories(){
            self::getConexion();

            $query = "SELECT * FROM category";

            $resultado = self::$conexion->prepare($query);
            $resultado->execute();

            while ($fila = $resultado->fetch()) {
                self::$categories[]=$fila;
             }     

            return self::$categories;
        }
    }
?>
