<?php 
    require_once "Conexion.php";

    class User
    {
        private static $conexion;

        public static function getConexion(){
            self::$conexion = Conexion::conectar();
        }

        public static function login($email, $password){

            self::getConexion();
            if(isset($email) && isset($password)){
                $query = "SELECT * FROM user WHERE email=:email AND password=:password";

                $resultado = self::$conexion->prepare($query);
                $resultado->bindValue(":email", $email);
                $resultado->bindValue(":password", $password);

                $resultado->execute();

                if($resultado->rowcount() > 0){
                    $filas = $resultado->fetch();
                    if($filas["email"] == $email && $filas["password"] == $password){
                        return true;
                    }
                }
                return false;
            }
        }

        public static function getUser($email, $password){
            self::getConexion();
            $query = "SELECT * FROM user WHERE email=:email AND password=:password";

            $resultado = self::$conexion->prepare($query);
            $resultado->bindValue(":email", $email);
            $resultado->bindValue(":password", $password);

            $resultado->execute();

            $filas = $resultado->fetch();

            $usuario = array(
                "user_id" => $filas["user_id"],
                "name" => $filas["name"],
                "lastname" => $filas["lastname"],
                "email" => $filas["email"]
            );

            return $usuario;
        }

        public static function register($name, $lastname, $email, $password){

            self::getConexion();
            if(isset($name) && isset($lastname) && isset($email) && isset($password)){
                $query = "INSERT INTO user (name, lastname, email, password, rol_id)VALUES(:name, :lastname, :email, :password, :rol_id)";

                $resultado = self::$conexion->prepare($query);

                $resultado->bindValue(":name", $name);
                $resultado->bindValue(":lastname", $lastname);
                $resultado->bindValue(":email", $email);
                $resultado->bindValue(":password", $password);
                $resultado->bindValue(":rol_id", 1);

                if($resultado->execute()){
                    return true;
                }
            }
        }
    }
?>      